sed -i "/^Terminal=/i Icon=share/icons/hicolor/scalable/apps/org.gnome.Geary.svg" ${INSTALL_DIR}/crackle.org.gnome.Geary.desktop
#a short description for the package
sed -i "s|@description@|geary is a mailclient|" ${INSTALL_DIR}/manifest.json

#adapt to fit the needs of the package
cp ${BUILD_DIR}/immodules.cache ${CLICK_LD_LIBRARY_PATH}/gtk-3.0/3.0.0/immodules/
sed -i "s/@CLICK_ARCH@/${ARCH_TRIPLET}/" ${CLICK_PATH}/geary.sh
sed -i "s/@CLICK_ARCH@/${ARCH_TRIPLET}/" ${CLICK_LD_LIBRARY_PATH}/gtk-3.0/3.0.0/immodules/immodules.cache
mv ${INSTALL_DIR}/crackle.org.gnome.Geary.desktop ${INSTALL_DIR}/geary.desktop
mv ${CLICK_LD_LIBRARY_PATH}/geary/* ${CLICK_LD_LIBRARY_PATH}
rmdir ${CLICK_LD_LIBRARY_PATH}/geary
